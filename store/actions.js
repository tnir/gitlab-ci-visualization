import axios from 'axios'
import { safeLoad } from 'yaml-ast-parser'

const keywords = [
  'image',
  'services',
  'stages',
  'types',
  'before_script',
  'after_script',
  'variables',
  'cache'
]

function removeParentNodeReferences(yamlAST) {
  if (!yamlAST) {
    return
  }

  const prototype = Object.getPrototypeOf(yamlAST)
  if (Object.is(prototype, Array.prototype)) {
    yamlAST.forEach(removeParentNodeReferences)
  } else if (Object.is(prototype, Object.prototype)) {
    yamlAST.parent = null
    Object.values(yamlAST).forEach(removeParentNodeReferences)
  }
}

function dereferenceAnchors(inputNode) {
  let { mappings } = inputNode.value
  if (!mappings) {
    return inputNode
  }

  const anchors = mappings
    .filter(node => node.value.referencesAnchor)
    .map(node => dereferenceAnchors(node.value))
  if (anchors.length < 1) {
    return inputNode
  }

  anchors.forEach(anchor => {
    mappings = mappings.concat(anchor.value.mappings)
  })

  return {
    ...inputNode,
    value: {
      ...inputNode.value,
      mappings
    }
  }
}

export default {
  createMermaidScript({ state, commit, dispatch }) {
    const { yamlAST } = state
    if (!yamlAST || !yamlAST.mappings) {
      return
    }

    const jobNodes = yamlAST.mappings
      .filter(node => {
        const jobName = node.key.value
        return !keywords.includes(jobName) && !jobName.startsWith('.')
      })
      .map(dereferenceAnchors)

    const stagesNode = yamlAST.mappings.find(
      node => node.key.value === 'stages'
    )
    const stageNames =
      stagesNode && stagesNode.value.items.map(item => item.value)
    const stages = (stageNames || ['build', 'test', 'deploy']).map(
      (stageName, stageIndex) => ({
        stageName,
        stageIndex,
        jobNames: []
      })
    )

    jobNodes.forEach(jobNode => {
      const stageNode =
        jobNode.value &&
        jobNode.value.mappings &&
        jobNode.value.mappings.find(node => node.key.value === 'stage')
      const stageName =
        (stageNode && stageNode.value && stageNode.value.value) || 'test'

      let stage = stages.find(stage => stage.stageName === stageName)
      if (!stage) {
        stage = {
          stageName,
          jobNames: []
        }
        stages.push(stage)
      }

      const jobName = jobNode.key.value.replace(/\d+[\s:\/\\]+\d+\s*$/, '')
      if (!stage.jobNames.includes(jobName)) {
        stage.jobNames.push(jobName)
      }
    })

    const createJobNode = stageIndex => (jobName, jobIndex) =>
      `        stage${stageIndex}_job${jobIndex}(${jobName});\n`

    let mermaidScript = 'graph LR;\n'
    stages.forEach(({ stageName, stageIndex, jobNames }) => {
      mermaidScript += `    subgraph ${stageName}\n`
      mermaidScript += `        stage${stageIndex}_connector(( ));\n`
      mermaidScript += jobNames.map(createJobNode(stageIndex)).join('')
      mermaidScript += `    end\n`

      if (stageIndex > 0) {
        mermaidScript += `    stage${stageIndex -
          1}_connector-->stage${stageIndex}_connector;\n`
      }
    })
    commit('STORE_MERMAID_SCRIPT', mermaidScript)
  },

  fetchYAMLFile({ commit, dispatch }, url) {
    return axios.get(url).then(({ data }) => {
      commit('STORE_YAML_CONTENT', data)
      dispatch('parseAST')
    })
  },

  parseAST({ state, commit, dispatch }) {
    const yamlAST = safeLoad(state.yamlContent)
    removeParentNodeReferences(yamlAST)
    commit('STORE_YAML_AST', yamlAST)
    dispatch('createMermaidScript')
  }
}
