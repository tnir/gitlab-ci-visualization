# gitlab-ci-visualization

> visualization prototype of .gitlab-ci.yml

see also https://gitlab.com/gitlab-org/gitlab/-/issues/15622

## Build Setup

``` bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn run dev

# build for production and launch server
$ yarn run build
$ yarn start

# generate static project
$ yarn run generate
```

For detailed explanation on how things work, checkout [Nuxt.js docs](https://nuxtjs.org).
